Stanley Gibbons Coding Challenge 6/25/2015

Author:   William Kappler
Email:    wekapple@ncsu.edu
Phone:    919-696-3879

=====================================

General Information
-------------------------------------
The purpose of this application is to calculate the mean, median, mode,
and range of a set of numbers passed via JSON.

All math functions are implemented in "math.php". Parsing and handling of
JSON data is handled in "mmmr.php". A .htaccess file is provided to enable
rewrite of the mmmr file to the proper endpoint, "/mmmr".

No installation is required. Just place mmmr.php, math.php, and .htaccess
in the same directory on an Apache-PHP server with rewrite support enabled.

Usage Information (JSON API)
-------------------------------------
The endpoint "/mmmr" accepts JSON requests via post. If an attribute called
"numbers" is passed, the API will process the array of numbers contained and
send a response called "results" containing the mean, median, mode, and range
of the numbers in the request array. All returned metrics are rounded to 3
decimal places.

Passing an empty "numbers" attribute will result in a response containing all
empty strings. For valid number arrays, any metric which does not exist is
returned as an empty string (this only happens for mode if more than one mode
exists).

Empty POST requests or GET requests will result in an Error 404 and a JSON
response called "error" containing the error information. Similarly, any
internal errors will be caught to allow the API to fail gracefully. On such a
failure, a JSON response called "error" will contain the error code and error
message provided by PHP. Note that warnings are caught as errors.

Passing non-numbers via JSON (or directly via the math library) is not
supported and will cause undefined behavior.

Usage Information (Math Library)
-------------------------------------
The math library may also be used directly by including "math.php" via PHP
itself. The functions provided are as follows:

function getMMMR( $numbers )
   Runs all provided mathematical operations on the array of numbers. This is
   intended to be the common interface to these functions.
   * $numbers: An array of numbers to be operated on.
   * Return value: Returns an array indexed on the names of the mathematical
   operations run; mean, median, mode, and range. If $numbers is empty,
   returns an array with the values set to null.

function getMean( $numbers )
   Finds the mean of $numbers
   * $numbers: An array of numbers to be operated on.
   * Return value: The mean (average) of the numbers passed. Returns null if
   $numbers if empty.

function getMedian( $numbers )
   Finds the median of $numbers.
   * $numbers: An array of numbers to be operated on.
   * Return value: Returns the median of the numbers passed. Note that if there
   is an even number of values in $numbers, this will be an average of the
   two middle numbers in the range. Returns null if $numbers if empty.

function getMode( $numbers )
   Finds the mode of $numbers.
   * $numbers: An array of numbers to be operated on.
   * Return value: Returns the mode (most common) number in $numbers if a
   single exists. Only one mode is ever returned; mathematically, there can be
   multiple modes to a set of numbers. If there is more than one mode in
   $numbers, this returns null. Returns null if $numbers if empty.

function getRange( $numbers )
   Finds the range of values in $numbers.
   * $numbers: An array of numbers to be operated on.
   * Return value: Returns the range of $numbers. If all numbers are the same,
   this is 0. Returns null if $numbers if empty.
