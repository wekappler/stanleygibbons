<?php
require_once("math.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    // This allows error handling to avoid cluttering the screen.
    ob_start();

    // Set an error handler to be able to gracefully fail, as well as
    // catch the error number and message.
    set_error_handler(function($errno, $errstr)
    {
        ob_end_clean();
        // Any error, return Error 500 with error message and number.
        header('HTTP/1.1 500 Internal Server Error');
        header("Content-Type: application/json");

        $response['error']['code']    = $errno;
        $response['error']['message'] = $errstr;
        exit(json_encode($response));
    });

    $inputPost = file_get_contents("php://input");
    $input     = json_decode($inputPost);
    unset($inputPost);

    $return            = array();
    $return['results'] = getMMMR($input->numbers);

    // Following the specs, find any nulls returned by MMMR and replace
    // them with "".
    foreach ($return['results'] as $key => $value)
    {
        if (is_null($return['results'][$key]))
        {
            $return['results'][$key] = "";
        }
    }

    header("Content-Type: application/json");
    exit(json_encode($return, JSON_PRETTY_PRINT));
}
else
{
    header("HTTP/1.0 404 Not Found");
    header("Content-Type: application/json");
    $response['error']['code']    = 404;
    $response['error']['message'] = "Method GET not available on this endpoint";
    exit(json_encode($response));
}

?>
