<?php
function getMean($numbers)
{
    if (!count($numbers))
    {
        return null;
    }

    $sum = 0;

    foreach ($numbers as $number)
    {
        $sum += $number;
    }

    return round(($sum / count($numbers)), 3);
}

function getMedian($numbers)
{
    if (!count($numbers))
    {
        return null;
    }

    sort($numbers, SORT_NUMERIC);
    $n = count($numbers);

    if ($n % 2)
    // Odd number
    {
        $i = ($n - 1) / 2;
        return round($numbers[$i], 3);
    }
    else
    // Even number
    {
        $i1 = ($n) / 2 - 1;
        return round((($numbers[$i1] + $numbers[$i1 + 1]) / 2), 3);
    }
}

// If more than one mode exist, this returns null.
function getMode($numbers)
{
    if (!count($numbers))
    {
        return null;
    }

    $freqs = array_count_values($numbers);

    // The following finds one of the possible modes and its frequency.
    $modeFreq = max($freqs);
    $mode     = array_search($modeFreq, $freqs);

    // This loop checks if other numbers exist with the same frequency as the
    // above 'mode' - if so, it returns null.
    foreach ($freqs as $number => $freq)
    {
        if (($freq >= $modeFreq) && ($number != $mode))
        {
            return null;
        }
    }

    // Otherwise, return the mode we got from the initial search.
    return round($mode, 3);
}

function getRange($numbers)
{
    // Don't attempt to operate on an empty number set.
    if (!count($numbers))
    {
        return null;
    }

    sort($numbers, SORT_NUMERIC);
    $nf = count($numbers) - 1;
    return round($numbers[$nf] - $numbers[0], 3);
}

function getMMMR($numbers)
{
    $return           = array();
    $return['mean']   = getMean($numbers);
    $return['median'] = getMedian($numbers);
    $return['mode']   = getMode($numbers);
    $return['range']  = getRange($numbers);
    return $return;
}
?>
